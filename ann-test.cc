#include <cstdio>
#include <cstdlib>

#include <chrono>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>

#include "net.hh"

using namespace std;

int main(int argc, char * argv[])
{
  const auto start_time = std::chrono::steady_clock::now();

  int i = 1;

  const char * inf = argv[i];
  i++;

  string modelf(argv[i]);
  i++;

  ANN::Lin3_Net nn {
    {10, 10},
    {10, 10},
    {10, 2}
  };
  nn.load(modelf);
  Vector input(nn.idim);
  Vector golden(nn.odim);
  string line;
  ifstream fin(inf);
  double e_total = 0;
  int nr_sample = 0;
  int ok = 0;
  while ( getline(fin, line)) {
    {
      stringstream ss(line);
      double d;
      for ( int i = 0; i < input.rows(); i++) {
        ss >> d;
        input[i] = d;
      }
      for ( int i = 0; i < golden.rows(); i++) {
        ss >> d;
        golden[i] = d;
      }
    }

    nn.feedforward( input, golden);
    //cout << "Out: " << nn.output << endl << "Loss: " << nn.loss << endl;
    e_total += nn.loss;
    if ( abs(nn.diff[0]) < 0.5 ) {
      ok++;
    }
    cout << "[" << nr_sample << "]\t" << golden << "\t" << nn.output << endl;
    nr_sample++;
  }
  printf("Samples [%05d], OK [%d] (%f), Loss [%f]\n", nr_sample, ok, ok/(double)nr_sample, e_total/nr_sample);

  long duration = std::chrono::duration_cast<std::chrono::milliseconds>
    (std::chrono::steady_clock::now() - start_time).count();
  printf( "%ld\n", duration);
  return 0;
}

