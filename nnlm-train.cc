#include <cstdio>
#include <cstdlib>
#include <csignal>

#include <chrono>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <vector>

#include "net.hh"

using namespace std;

volatile bool exitnow = false;

extern "C" {
  void sigINT_handle(int para)
  {
    exitnow = true;
  }
}


int main(int argc, char * argv[])
{
  void (*prev_handler)(int);
  prev_handler = signal (SIGINT, sigINT_handle);

  const auto start_time = std::chrono::steady_clock::now();

  int i = 1;

  const int bsize = atoi(argv[i]);
  i++;
  const int NR_GRAM = atoi(argv[i]);
  i++;
  const int PROJ_SZ = atoi(argv[i]);
  i++;
  const int HIDDEN_SZ = atoi(argv[i]);
  i++;
  const int VOCAB_LIM = atoi(argv[i]);
  i++;
  const int VOCAB_SHORT = atoi(argv[i]);
  i++;
  ANN::NNLM_Net nn {
    {bsize, NR_GRAM, VOCAB_LIM, PROJ_SZ},
    {bsize, NR_GRAM*PROJ_SZ, HIDDEN_SZ},
    {bsize, HIDDEN_SZ, VOCAB_SHORT}
  };

  const char * inf = argv[i];
  i++;

  double threshold = atof(argv[i]);
  i++;

  string modelf(argv[i]);
  i++;

  double MAX_ITER = atof(argv[i]);
  i++;

  double rate0 = atof(argv[i]);
  double rate = rate0;
  i++;

  double DECAY = atof(argv[i]);
  i++;

  double RATE_MULTI = atof(argv[i]);
  i++;

  string load_model (argv[i]);
  i++;

  int START = 0;
  if ( load_model == "load" ) {
    string modelf(argv[i]);
    i++;
    nn.load(modelf);
    START = atoi(argv[i]);
    i++;
  }

  nn.decay = DECAY;
  nn.step(rate, rate);
  string line;
  int iter = 0;
  int t = 0;
  Matrix input(NR_GRAM, bsize);
  Matrix golden(1, bsize);
  while ( iter < MAX_ITER) {
    double e_total = 0;
    int nr_sample = 0;
    int line_nr = 0;
    ifstream fin(inf);
    int i = 0;
    for ( i = 0; i < START; i++) {
      //skip
      getline(fin, line);
    }
    {
      long duration = std::chrono::duration_cast<std::chrono::milliseconds>
        (std::chrono::steady_clock::now() - start_time).count();
      printf("Elapsed time: [%ld], done loading\n", duration);
    }
    START = 0;
    int b = 0;
    while ( getline(fin, line)) {
      {
        stringstream ss(line);
        double d;
        for ( int n = 0; n < NR_GRAM; n++) {
          ss >> d;
          input(n, b) = d;
        }
        ss >> d;
        golden(0,b) = d;
        b++;
      }
      {
        line_nr++;
        i++;
        t++;
        nr_sample++;
        if ( (line_nr % 1024) == 0 )
        {
          long duration = std::chrono::duration_cast<std::chrono::milliseconds>
            (std::chrono::steady_clock::now() - start_time).count();
          printf("Elapsed time: [%ld]\t, Sentences [%05d], Iter [%03d], Rate [%0.10f], Loss [%f]\n", duration, line_nr, iter, rate, e_total/nr_sample);
        }
      }
      if ( b == bsize ) {
        b = 0;
        nn.feedforward( input, golden);
        //cout << "Out: " << nn.output << "\tLoss: " << nn.loss << endl;
        //cout << "Loss: " << nn.loss << endl;
        e_total += nn.loss;
        nn.backprop();
        rate = rate0/ (1+RATE_MULTI*t*bsize);
        nn.step(rate, rate);

        if ( exitnow ) {
          cout << endl;
          long duration = std::chrono::duration_cast<std::chrono::milliseconds>
            (std::chrono::steady_clock::now() - start_time).count();
          printf("Elapsed time: [%ld]\t, Sentences [%05d], Iter [%03d], Rate [%0.10f], Loss [%f]\n", duration, line_nr, iter, rate, e_total/nr_sample);
          cout << "Next [" << i << "]" << endl;
          break;
        }
      }
    }
    if ( exitnow || e_total/nr_sample < threshold ) {
      break;
    }
    iter++;
  }

  long duration = std::chrono::duration_cast<std::chrono::milliseconds>
    (std::chrono::steady_clock::now() - start_time).count();
  printf( "%ld\n", duration);
  cerr << "Saving model to: " << modelf << "..." << endl;
  nn.save(modelf);
  cerr << "Done" << endl;
  signal (SIGINT, prev_handler);
  return 0;
}

