#include <cstdio>
#include <cstdlib>
#include <csignal>

#include <chrono>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>

#include "net.hh"

using namespace std;

int main(int argc, char * argv[])
{
  int i = 1;
  const auto start_time = std::chrono::steady_clock::now();

  const int bsize = atoi(argv[i]);
  i++;
  const int NR_GRAM = atoi(argv[i]);
  i++;
  const int PROJ_SZ = atoi(argv[i]);
  i++;
  const int HIDDEN_SZ = atoi(argv[i]);
  i++;
  const int VOCAB_LIM = atoi(argv[i]);
  i++;
  const int VOCAB_SHORT = atoi(argv[i]);
  i++;
  ANN::NNLM_Net nn {
    {bsize, NR_GRAM, VOCAB_LIM, PROJ_SZ},
      {bsize, NR_GRAM*PROJ_SZ, HIDDEN_SZ},
      {bsize, HIDDEN_SZ, VOCAB_SHORT}
  };

  const char * inf = argv[i];
  i++;

  string modelf(argv[i]);
  i++;

  nn.load(modelf);
  long duration = std::chrono::duration_cast<std::chrono::milliseconds>
    (std::chrono::steady_clock::now() - start_time).count();
  fprintf(stderr, "Elapsed [%ld], done loading.\n", duration);

  Matrix input(NR_GRAM,bsize);
  Matrix golden(1,bsize);

  ifstream fin(inf);
  double e_total = 0;
  double e_line = 0;
  int nr_sample = 0;
  int line_nr = 0;
  int sent_nr = 0;

  string line;
  int b = 0;
  while ( getline(fin, line)) {
    if ( line.empty() )
    {
      e_total += e_line;
      e_line = 0;
      line_nr = 0;
      sent_nr++;
      if ( sent_nr % 100 == 0) {
        long duration = std::chrono::duration_cast<std::chrono::milliseconds>
          (std::chrono::steady_clock::now() - start_time).count();
        fprintf(stderr, "Elapsed [%ld], #%d Sentences.\n", duration, sent_nr);
      }
      continue;
    }
    {
      stringstream ss(line);
      double d;
      for ( int n = 0; n < NR_GRAM; n++) {
        ss >> d;
        input(n, b) = d;
      }
      ss >> d;
      golden(0,b) = d;
      b++;
    }
    {
      line_nr++;
      nr_sample++;
    }
    if ( b == bsize ) {
      nn.feedforward( input, golden);
      for ( int c = 0; c < b; c++ ) {
        int i =  golden(0,c);
        if ( i >= nn.output.rows() ) {
          i = 1; //RARE
        }
        printf("%0.32f\n", std::log(nn.output(i, c)));
      }
      b = 0;
    }
  }
  fprintf(stderr, "[%d] Samples.\n", nr_sample);
  fprintf(stderr, "[%d] left.\n", b);
  if ( b < bsize ) {
    nn.feedforward( input, golden);
    for ( int c = 0; c < b; c++ ) {
      int i =  golden(0,c);
      if ( i >= nn.output.rows() ) {
        i = 1; //RARE
      }
      printf("%0.32f\n", std::log(nn.output(i, c)));
    }
    b = 0;
  }

  return 0;
}

