#include <cstdio>
#include <cstdlib>

#include <chrono>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>

#include "net.hh"

using namespace std;

int main(int argc, char * argv[])
{
  ANN::Lin3_Net nn {
    {10, 10},
    {10, 10},
    {10, 2}
  };
  const auto start_time = std::chrono::steady_clock::now();

  int i = 1;

  const char * inf = argv[i];
  i++;

  double threshold = atof(argv[i]);
  i++;

  string modelf(argv[i]);
  i++;

  double MAX_ITER = atof(argv[i]);
  i++;

  double rate = atof(argv[i]);
  i++;

  string load_model (argv[i]);
  i++;

  if ( load_model == "load" ) {
    string modelf(argv[i]);
    i++;
    nn.load(modelf);
  }

  nn.step(rate, rate);
  Vector input(nn.idim);
  Vector golden(nn.odim);
  string line;
  int iter = 1;
  while ( iter < MAX_ITER) {
    ifstream fin(inf);
    double e_total = 0;
    int nr_sample = 0;
    while ( getline(fin, line)) {
      {
        stringstream ss(line);
        double d;
        for ( int i = 0; i < input.rows(); i++) {
          ss >> d;
          input[i] = d;
        }
        for ( int i = 0; i < golden.rows(); i++) {
          ss >> d;
          golden[i] = d;
        }
      }

      nn.feedforward( input, golden);
      //cout << "Out: " << nn.output << "\tLoss: " << nn.loss << endl;
      e_total += nn.loss;
      Eigen::internal::set_is_malloc_allowed(false);
      nn.backprop();
      Eigen::internal::set_is_malloc_allowed(true);
      nr_sample++;
    }
    printf("Samples [%05d], Iter [%03d], Loss [%f]\n", nr_sample, iter, e_total/nr_sample);
    iter++;
    if ( e_total/nr_sample < threshold ) {
      break;
    }
  }

  long duration = std::chrono::duration_cast<std::chrono::milliseconds>
    (std::chrono::steady_clock::now() - start_time).count();
  printf( "%ld\n", duration);
  nn.save(modelf);
  return 0;
}

