#include <cstdio>
#include <cstdlib>
#include <csignal>

#include <chrono>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <vector>

#include "net.hh"

using namespace std;

constexpr int HIDDEN_SZ = 128;
constexpr int NR_GRAM = 2;
constexpr int VOCAB_LIM = 4*1024;

bool exitnow = false;

extern "C" {
  void sigINT_handle(int para)
  {
    exitnow = true;
  }
}

int main(int argc, char * argv[])
{
  ANN::NNLM_Net nn {
    {NR_GRAM, VOCAB_LIM, HIDDEN_SZ},
      {NR_GRAM*HIDDEN_SZ, NR_GRAM*HIDDEN_SZ},
      {NR_GRAM*HIDDEN_SZ, VOCAB_LIM}
  };
  void (*prev_handler)(int);
  prev_handler = signal (SIGINT, sigINT_handle);

  const auto start_time = std::chrono::steady_clock::now();

  int i = 1;

  const char * inf = argv[i];
  i++;

  double threshold = atof(argv[i]);
  i++;

  string modelf(argv[i]);
  i++;

  double MAX_ITER = atof(argv[i]);
  i++;

  double rate0 = atof(argv[i]);
  double rate = rate0;
  i++;

  double DECAY = atof(argv[i]);
  i++;

  double RATE_MULTI = atof(argv[i]);
  i++;

  string load_model (argv[i]);
  i++;

  int START = 0;
  if ( load_model == "load" ) {
    string modelf(argv[i]);
    i++;
    nn.load(modelf);
    START = atoi(argv[i]);
    i++;
  }

  nn.decay = DECAY;
  nn.step(rate, rate);
  string line;
  int iter = 1;
  int t = 0;
  vector<Eigen::VectorXd> input;
  vector<Eigen::VectorXd> golden;
  {
    ifstream fin(inf);
    while ( getline(fin, line)) {
      Vector input_(2);
      Vector golden_(1);
      stringstream ss(line);
      double d;
      for ( int i = 0; i < input_.rows(); i++) {
        ss >> d;
        input_[i] = d;
      }
      ss >> d;
      golden_[0] = d;
      input.push_back(input_);
      golden.push_back(golden_);
    }
    long duration = std::chrono::duration_cast<std::chrono::milliseconds>
      (std::chrono::steady_clock::now() - start_time).count();
    printf("Elapsed time: [%ld], done loading\n", duration);
  }
  while ( iter < MAX_ITER) {
    double e_total = 0;
    int nr_sample = 0;
    int line_nr = 0;
    for ( int i = START; i<input.size(); i++) {
      {
        line_nr++;
        if ( (line_nr % 1000) == 0 )
        {
          long duration = std::chrono::duration_cast<std::chrono::milliseconds>
            (std::chrono::steady_clock::now() - start_time).count();
          printf("Elapsed time: [%ld]\t, Sentences [%05d], Iter [%03d], Rate [%0.10f], Loss [%f]\n", duration, line_nr, iter, rate, e_total/nr_sample);
        }
      }
      t++;

      Eigen::internal::set_is_malloc_allowed(false);
      nn.feedforward( input[i], golden[i]);
      Eigen::internal::set_is_malloc_allowed(true);
      //cout << "Out: " << nn.output << "\tLoss: " << nn.loss << endl;
      //cout << "Loss: " << nn.loss << endl;
      e_total += nn.loss;
      Eigen::internal::set_is_malloc_allowed(false);
      nn.backprop();
      Eigen::internal::set_is_malloc_allowed(true);
      rate = rate0/ (1+RATE_MULTI*t);
      nn.step(rate, rate);

      nr_sample++;
      if ( exitnow ) {
        cout << endl;
        long duration = std::chrono::duration_cast<std::chrono::milliseconds>
          (std::chrono::steady_clock::now() - start_time).count();
        printf("Elapsed time: [%ld]\t, Sentences [%05d], Iter [%03d], Rate [%0.10f], Loss [%f]\n", duration, line_nr, iter, rate, e_total/nr_sample);
        cout << "Next [" << i << "]" << endl;
        break;
      }
    }
    if ( exitnow || e_total/nr_sample < threshold ) {
      break;
    }
    iter++;
  }

  long duration = std::chrono::duration_cast<std::chrono::milliseconds>
    (std::chrono::steady_clock::now() - start_time).count();
  printf( "%ld\n", duration);
  cerr << "Saving model to: " << modelf << "..." << endl;
  nn.save(modelf);
  cerr << "Done" << endl;
  return 0;
}

