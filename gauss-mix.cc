#include <iostream>
#include <random>

using namespace std;

int main(int argc, char * argv[])
{
  int i = 1;

  const int nrolls = atoi(argv[i]);
  i++;

  const int nmix = atoi(argv[i]);
  i++;

  const double miu0 = atof(argv[i]);
  i++;

  const double sigma0 = atof(argv[i]);
  i++;

  const double miu1 = atof(argv[i]);
  i++;

  const double sigma1 = atof(argv[i]);
  i++;

  std::default_random_engine generator;
  std::normal_distribution<double> d0(miu0, sigma0);
  std::normal_distribution<double> d1(miu1, sigma1);


  for (int i=0; i<nrolls; ++i) {
    for ( int j = 0; j<nmix; j++ ) {
      cout << d0(generator) << "\t";
    }
    cout <<  1 << "\t" << 0 << endl;
  }

  for (int i=0; i<nrolls; ++i) {
    for ( int j = 0; j<nmix; j++ ) {
      cout << d1(generator) << "\t";
    }
    cout <<  0 << "\t" << 1 << endl;
  }

  return 0;
}
