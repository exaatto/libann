COMFLAGS=-Wall
#COMFLAGS+=-ggdb
COMFLAGS+=-O3
COMFLAGS+=-DMATRIX_USE_EIGEN
#COMFLAGS+=-pg
COMFLAGS+=-mtune=corei7-avx -mavx
COMFLAGS+=-fopenmp
COMFLAGS+=-DEIGEN_RUNTIME_NO_MALLOC

CXXFLAGS=$(COMFLAGS)
CXXFLAGS+=-I$(HOME)/src/eigen-hg/
#CXXFLAGS+=-I$(HOME)/usr/local/include/eigen3/
CXXFLAGS+=-std=c++1y

LDFLAGS=$(COMFLAGS)

LOADLIBES=-L$(HOME)/usr/local/lib/
LOADLIBES+=-L$(HOME)/usr/local/lib64/

CC=g++
CXX=g++
LDLIBS=-lstdc++
LDLIBS=-lboost_serialization

BIN=gauss-mix nnlm-train nnlm-ppl
#BIN+=ann-train ann-test

all: $(BIN)

clean:
	$(RM) $(BIN) *.o
