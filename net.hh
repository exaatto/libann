#ifndef NET_HH
#define NET_HH

#include <vector>
#include <string>
#include <memory>
#include <numeric>
#include <cmath>


#include <fstream>
#include <iostream>

#include <Eigen/Dense>
typedef Eigen::MatrixXd Matrix;
typedef Eigen::VectorXd Vector;

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

#include "titerator/titerator.hh"

  template <typename T>
void pstat( const std::string & s, const T & m )
{
  std::cerr << "\e[1;31m"
    << "STAT ("
    << s
    << ")\t"
    << "\e[m"

    << "min ["
    << "\e[1;32m"
    << m.minCoeff()
    << "\e[m"
    << "]\t"

    << "avg ["
    << "\e[1;32m"
    << m.array().sum()/(m.rows()*m.cols())
    << "\e[m"
    << "]\t"

    << "max ["
    << "\e[1;32m"
    << m.maxCoeff()
    << "\e[m"
    << "]"
    << std::endl;
}
namespace ANN {

  template <typename Activation>
    struct NonBiasedLayer
    {
      double step_omega;

      Matrix omega;

      Vector input;
      Vector sensitivity;

      Activation activition;

      NonBiasedLayer( int idim, int odim, bool randinit = true)
        :omega(odim, idim),
        input(idim),
        sensitivity(odim),
        activition(idim, odim)
      {
        if ( randinit ) {
          omega = Matrix::Random(odim, idim);
        }
      }
      ~NonBiasedLayer()
      {}

      template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
          ar & boost::serialization::make_array(omega.data(), omega.size());
          ar & activition;
        }

      void step( double so, double sb)
      {
        step_omega = so;
      }
      void transfer(Vector & out) {
        out.noalias() = omega*input;
        activition.activate(out);
      }
      void update_param()
      {
        omega.noalias() -= step_omega*(sensitivity*input.adjoint());
      }

    };
  template <typename Activation>
    struct ProjectLayer
    {
      int bsize;
      int n_words;
      double step_omega;

      Matrix omega;

      Matrix input;
      Matrix sensitivity;
      Matrix sensitivity_prev;

      Activation activition;


      ProjectLayer(int bsize, int n_words, int idim, int odim, bool randinit = true)
        :bsize(bsize),
        n_words(n_words),
        omega(odim, idim),
        input(n_words, bsize),
        sensitivity(n_words*odim, bsize),
        sensitivity_prev(n_words*odim, bsize),
        activition(idim, odim)
      {
        if ( randinit ) {
          omega = Matrix::Random(odim, idim);
        }
        sensitivity_prev.setZero(n_words*odim, bsize);
      }
      ~ProjectLayer()
      {}

      template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
          ar & boost::serialization::make_array(omega.data(), omega.size());
          ar & activition;
        }

      void reduce()
      {
        bsize = 1;
        input.resize(n_words, 1);
      }
      void step( double so, double sb)
      {
        step_omega = so;
      }
      void transfer(Matrix & out) {
        int odim = omega.rows();
        for ( int b = 0; b < bsize; b++) {
          for ( int i = 0; i < n_words; i++) {
            out.col(b).segment(i*odim, odim) = omega.col(input(i,b));
          }
        }
        activition.activate(out);
      }
      void update_param()
      {
        int odim = omega.rows();
        sensitivity_prev.array() = 0.1*sensitivity_prev.array() + sensitivity.array();
        for ( int b = 0; b < bsize; b++) {
          for ( int i = 0; i < n_words; i++) {
            int idx = input(i,b);
            omega.col(idx).array() -= step_omega*(sensitivity_prev.col(b).segment(i*odim, odim).array());
            //omega.col(idx).array() -= step_omega*(sensitivity.segment(i*odim, odim).array());
          }
        }
      }

    };
  template <typename Activation>
    struct Layer
    {
      int bsize;
      double step_omega, step_beta;

      Matrix omega;
      Matrix beta;

      Matrix input;
      Matrix sensitivity;
      Matrix sensitivity_prev;

      Activation activition;

      Layer( int bsize, int idim, int odim, bool randinit = true)
        :bsize(bsize),
        omega(odim, idim),
        beta(odim, bsize),
        input(idim, bsize),
        sensitivity(odim, bsize),
        sensitivity_prev(odim, bsize),
        activition(idim, odim)
      {
        if ( randinit ) {
          omega = Matrix::Random(odim, idim);
          beta.col(0) = Vector::Random(odim);
          for ( int i = 1; i < bsize; i++ ) {
            beta.col(i) = beta.col(0);
          }
        }
        sensitivity_prev.setZero(odim, bsize);
      }
      ~Layer()
      {}

      template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
          ar & boost::serialization::make_array(omega.data(), omega.size());
          ar & boost::serialization::make_array(beta.data(), beta.size());
          ar & activition;
        }

      void reduce()
      {
        bsize = 1;
        input.resize(input.rows(), 1);
        beta = beta.rowwise().mean();
      }
      void step( double so, double sb)
      {
        step_omega = so;
        step_beta = sb;
      }
      void transfer(Matrix& out) {
        out = beta;
        out.noalias() += omega*input;
        activition.activate(out);
      }
      void update_param()
      {
        sensitivity_prev.array() = 0.1*sensitivity_prev.array() + sensitivity.array();
        omega.noalias() -= step_omega*(sensitivity_prev*input.adjoint());
        beta.noalias() -= step_beta*sensitivity_prev;
        //omega.noalias() -= step_omega*(sensitivity*input.adjoint());
        //beta.noalias() -= step_beta*sensitivity;
      }

    };
  namespace Activation {
    struct Linear
    {
      Linear(int idim, int odim)
      {}
      void activate(Matrix & out)
      {}
      void backprop(Matrix & sens, Matrix& out)
      {}
      template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {}
    };
    struct Sigmoid
    {
      Sigmoid(int idim, int odim)
      {}
      void activate(Matrix & out)
      {
        for ( int i = 0; i<out.cols(); i++) {
          out.col(i) = out.col(i).array().exp();
          out.col(i) = out.col(i).array().inverse();
          out.col(i).array() += 1;
          out.col(i) = out.col(i).array().inverse();
        }
      }
      void backprop(Vector & sens, Vector & out)
      {
        sens = sens.array() * out.array()*(1-out.array());
      }
      template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {}
    };
    struct TanH
    {
      TanH(int idim, int odim)
      {}
      void activate(Matrix & out)
      {
        for ( int i = 0; i < out.cols(); i++) {
          out.col(i).array() = (2*out.col(i).array()).exp();
          out.col(i).array() = ( out.col(i).array()-1 ) / ( out.col(i).array()+1 );
        }
        //out = (out.array().exp() - (0-out.array()).exp()) / (out.array().exp() + (0-out.array()).exp());
      }
      void backprop(Matrix & sens, Matrix & out)
      {
        out.array() = out.array().square();
        sens = sens.array() * (1-out.array());
      }
      template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {}
    };
    struct SoftMax
    {
      Matrix J;
      SoftMax(int idim, int odim)
        :J(odim, odim)
      {
      }
      void activate(Vector & out)
      {
        out.array() = out.array().exp();
        out.array() /= out.array().sum();
      }
      void backprop(Vector & sens, Vector & out)
      {
        J.noalias() = out*out.adjoint();
        J -= out.asDiagonal();
        J.array() = 0 - J.array();
        sens = J*sens;
      }
      template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {}
    };
    struct LMSoftMax
    {
      LMSoftMax(int idim, int odim)
      {
      }
      void activate(Matrix & out)
      {
        for ( int i = 0; i < out.cols(); i++) {
          out.col(i) = (out.col(i).array() - std::log(out.col(i).array().exp().sum())).exp();
        }
      }
      void backprop(Vector & sens, Vector & out)
      {
      }
      template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {}
    };
  }
  typedef NonBiasedLayer<Activation::Linear> NonBiasedPureLinLayer;
  typedef ProjectLayer<Activation::Linear> LinProjectLayer;
  typedef Layer<Activation::Linear> PureLinLayer;
  typedef Layer<Activation::Sigmoid> SigmoidLayer;
  typedef Layer<Activation::SoftMax> SoftMaxLayer;
  typedef Layer<Activation::LMSoftMax> LMSoftMaxLayer;
  typedef Layer<Activation::TanH> TanHLayer;

  namespace Internal {
    struct feedforward
    {
      template <std::size_t I, typename T>
        void iterator(T & layers) const
        {
          std::get<I>(layers).transfer( std::get<I+1>(layers).input);
        }
    };
    struct backprop
    {
      template <std::size_t I, typename T>
        void iterator(T & layers) const
        {
          auto & self =  std::get<I>(layers);
          auto & next =  std::get<I+1>(layers);
          self.sensitivity.noalias() = next.omega.adjoint()*next.sensitivity;
          self.activition.backprop(self.sensitivity, next.input);
          self.update_param();
        }
    };
    struct set_step
    {
      double step_omega, step_beta;
      set_step(double step_omega, double step_beta)
        :step_omega(step_omega),
        step_beta(step_beta)
      {}
      template <typename L>
        void operator() (L& l) const
        {
          l.step(step_omega, step_beta);
        }
    };
    struct reduce_each
    {
      template <typename L>
        void operator() (L& l) const
        {
          l.reduce();
        }
    };
    template < typename Ar>
      struct ar_each
      {
        Ar &ar;
        ar_each( Ar & ar)
          :ar(ar)
        {}
        template <typename L>
          void operator() (L& l) const
          {
            ar & l;
          }
      };

  };

  template<typename ... AllLayers>
    struct Net
    {
      std::tuple< AllLayers...> layers;
      int idim, odim;
      Matrix output;
      Matrix golden;
      double loss;

      static constexpr long layer_level = sizeof...(AllLayers);

      int get_idim()
      {
        return std::get<0>(layers).input.rows();
      }
      int get_odim()
      {
        return std::get<layer_level-1>(layers).beta.rows();
      }

      Net(AllLayers... layers)
        :layers(layers...),
        idim(get_idim()),
        odim(get_odim()),
        loss( std::numeric_limits<double>::max() )
      {
      }
      virtual double lossf( Matrix & output) = 0;
      virtual void loss_gradf(Matrix & sens) = 0;
      void feedforward(const Matrix & _input, const Matrix & _golden)
      {
        golden = _golden;
        std::get<0>(layers).input = std::move(_input);
        tuple_iteration::tuple_iterator<0,layer_level-1>::iterator_i(layers, Internal::feedforward());
        std::get<layer_level-1>(layers).transfer(output);
        loss = lossf(output);
      }
      void backprop()
      {
        auto & last_layer = std::get<layer_level-1>(layers);
        loss_gradf(last_layer.sensitivity);
        last_layer.activition.backprop(last_layer.sensitivity, output);
        last_layer.update_param();
        tuple_iteration::tuple_iterator<0,layer_level-1>::riterator_i(layers, Internal::backprop());
      }
      void step( double step_omega, double step_beta)
      {
        tuple_iteration::for_each( layers, Internal::set_step(step_omega, step_beta) );
      }
      template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
          tuple_iteration::for_each( layers, Internal::ar_each<Archive>(ar) );
        }
      void save( std::string filename, bool bin = false)
      {
        std::ofstream ofs(filename);
        if ( bin ) {
          boost::archive::binary_oarchive oa(ofs);
          oa << *this;
        } else {
          boost::archive::text_oarchive oa(ofs);
          oa << *this;
        }
      }
      void load( std::string filename, bool bin = false)
      {
        std::ifstream ifs(filename);
        if ( bin ) {
          boost::archive::binary_iarchive ia(ifs);
          ia >> *this;
        } else {
          boost::archive::text_iarchive ia(ifs);
          ia >> *this;
        }
      }
    };

  template<typename ... AllLayers>
    struct FeedForwardNet
    :public Net<AllLayers...>
    {
      using  Net<AllLayers...>::Net;
    };

  template<typename ... AllLayers>
    struct LMS_FF_Net
    :public FeedForwardNet<AllLayers...>
    {
      Vector diff;
      typedef FeedForwardNet<AllLayers...> _Base_T;
      LMS_FF_Net(AllLayers ... ls)
        :FeedForwardNet<AllLayers...>(ls...),
        diff(_Base_T::odim)
      {
        _Base_T::golden = Vector::Zero(_Base_T::odim);
      }

      virtual double lossf( Vector & output)
      {
        diff = _Base_T::golden - output;
        return diff.squaredNorm();
      }
      virtual void loss_gradf( Vector & sens)
      {
        sens = -2 * ( diff);
      }
    };

  template<typename ... AllLayers>
    struct LM_Net
    :public FeedForwardNet<AllLayers...>
    {
      double decay;
      typedef FeedForwardNet<AllLayers...> _Base_T;
      LM_Net(AllLayers ... ls)
        :FeedForwardNet<AllLayers...>(ls...)
      {}

      void reduce_minibatch()
      {
        tuple_iteration::for_each( _Base_T::layers, Internal::reduce_each() );
      }
      virtual double lossf( Matrix & output)
      {
        double loss = 0;
        for ( int c = 0; c < _Base_T::golden.cols(); c++ ) {
          int i =  _Base_T::golden(0,c);
          if ( i >= _Base_T::output.rows() ) {
            i = 1; //RARE
          }
          loss -= std::log( output(i, c));
        }
        return loss;
      }
      virtual void loss_gradf( Matrix & sens)
      {
        sens.setZero();
        for ( int j = 0; j < _Base_T::golden.cols(); j++) {
          int i =  _Base_T::golden(0,j);
          if ( i >= _Base_T::output.rows() ) {
            i = 1; //RARE
          }
          sens(i,j) = _Base_T::output(i,j)-1;
        }
      }
      void backprop()
      {
        auto & smax_layer = std::get<2>(_Base_T::layers);
        auto & tanh_layer = std::get<1>(_Base_T::layers);
        auto & proj_layer = std::get<0>(_Base_T::layers);
        loss_gradf(smax_layer.sensitivity);
        smax_layer.omega.array() -= smax_layer.step_omega*decay*2*smax_layer.omega.array();
        smax_layer.update_param();

        tanh_layer.sensitivity.noalias() = smax_layer.omega.adjoint()*smax_layer.sensitivity;
        tanh_layer.activition.backprop(tanh_layer.sensitivity,smax_layer.input);
        tanh_layer.omega.array() -= tanh_layer.step_omega*decay*2*tanh_layer.omega.array();
        tanh_layer.update_param();

        proj_layer.sensitivity.noalias() = tanh_layer.omega.adjoint()*tanh_layer.sensitivity;
        proj_layer.activition.backprop(proj_layer.sensitivity,tanh_layer.input);
        proj_layer.update_param();
      }
    };

  typedef LMS_FF_Net<PureLinLayer, TanHLayer, SoftMaxLayer> Lin3_Net;
  typedef LM_Net<LinProjectLayer, TanHLayer, LMSoftMaxLayer> NNLM_Net;
}
#endif
